Connectionless protocol documentation
GMP

Theory of communication

+-+-+-Joining the game-+-+-+

Server is listening for any join requests
Client sends 	O=CON, A=""
Server response	O=CON, A=ACK

Client sends ID Request
Client sends 	O=RID, A=""
Server response O=RID, A=ACK
		O=RID, A=SID, I=ClientID
Client response O=RIN, A=ACK, I=ClientID

Server waits for all players to send RDY
Client sends 	O="RDY", A="", I=ClientID
Server response	O="RDY", A="ACK"

Client wait for everything to set up.
Server sends	O="STR", A="", I=ClientID
Client response	O="STR", A="ACK"

+++++Game started+++++

Time info
Server sends info about how much time left each 10s
Server sends	O = "TLF", A = "", I = ClientID, L = remainTime
Client response	O = "TLF", A = "ACK"

Number sending
Client sends 	O = "NUM", A = "", I = ClientID, L = number
Server response O = "NUM", A = "ACK", I = ClientID

When client hit number
Server sends to - winner O = "HIT", A = "", I = ClientID
		- losers O = "LOS", A = "", I = ClientID
Client response	- winner O = "HIT", A = "ACK"
		- losers O = "LOS", A = "ACK"

When client miss number
Server sends 	O = "NUM", A = "MIS", I = ClientID, L = number
Client response O = "NUM", A = "ACK"

Server sends to ALL CLIENTS
{
	TIME'S UP only when noone wins
	Server sends O = "TUP", A = "", I = ClientID
	Client response O = "TUP", A = "ACK"

	All lose
	Server sends O = "ALS", A = "", I = ClientID
	Client response O = "ALS", A = "ACK"
}

-----Closing connection and finishing game-----
Server waits for players to send finish request
Client sends 	O = "FIN", A="", I=ClientID
Server response O = "FIN", A="ACK", I=ClientID
		O = "FIN", A="END", I=ClientID
Client response O = "FIN", A="ACK", I=ClientID

When all clients quit server is cleaning up and shutting down.

Options & answers
0000000 CON
	00000 ACK
	00001 ERR
0000001	RID
	00000 ACK
	00001 SID
0000010	RDY
	00000 ACK
0000011	STR
	00000 ACK
0000100 TLF
	00000 ACK
0000101 NUM
	00000 ACK
	00001 MIS	
0000110 HIT
	00000 ACK
0000111 LOS
	00000 ACK
0001000	TUP
	00000 ACK
0001001 ALS
	00000 ACK
0001010 FIN
	00000 ACK
	00001 END