#include "server.h"
#include "client.h"

void Server::main() {
	std::cout << "serwer";
	
	//Losowanie liczby
	srand(time(NULL));
	randNumb = rand() % 254 + 1;
	std::cout << "\nWylosowana liczba: " << randNumb << "\n";
	
	//Initialize winsock
	wsData;
	ver = MAKEWORD(2, 2);
	wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0) {
		std::cerr << "Cant start winsock! \n";
		return;
	}
	//Create a listening socket
	ListeningSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (ListeningSocket == INVALID_SOCKET) {
		std::cerr << "Cant create a socket! Quitting... \n";
		return;
	}

	//Bind the socket to an ip address and port
	serverHint.sin_addr.S_un.S_addr = ADDR_ANY;
	serverHint.sin_family = AF_INET;
	serverHint.sin_port = htons(53333);
	if (bind(ListeningSocket, (sockaddr*)&serverHint, sizeof(serverHint)) == SOCKET_ERROR) {
		std::cout << "Can't bind socket! Error: " << WSAGetLastError() << "\n";
	}

	std::cout << "\nWszystko ruszylo! Czekam na graczy\n";
	
	tmp.clientLength = sizeof(tmp.clAddr);
	ZeroMemory(&tmp.clAddr,tmp.clientLength);

	//Enter the loop for joining players
	while (READY == false) {
		//std::cout << "Petla no.1!\n";
		//Adding players
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, &tmp.clAddr, &tmp.clientLength);
		unboxPacket();
		PrintData();
		//std::cout << "\nodebralem O " << O << " A " << A << "!\n";
		if (O == "CON")
		{
			sendACK(tmp);
		}
		if (O == "RID") 
		{
			std::cout << "\nodebralem O " << O << " A " << A << "!\n";
			if (A != "ACK")
			{
				O = "RID";
				sendACK(tmp);
				addPlayers(tmp);
			}
		}
		if (O == "RDY") {
			sendACK(tmp);
			ready++;
			if (ready == play) {
				READY = true;
				break;
			}
		}
		else {
			//do nothing it's probably ack
		}
		if (bytesRecieved == SOCKET_ERROR)
		{
			std::cerr << "Error in recvfrom(): " << WSAGetLastError() << " Quitting... \n";
		}
	//End of adding players loop
	}

	//Time calculating
	using namespace std::literals::chrono_literals;
	beginTime = currTime();
	int tmpInt = 0;
	for (int i = 0; i < players.size() - 1; i++)
	{
		tmpInt += players[i].id;
	}
	durTime = ((tmpInt) * 99) % 100 + 30;
	std::cout << "Time of the game " << durTime << "\n";
	remainTime = durTime;

	//Sending start message to clients
	std::cout << "Mam graczy! Wysy�am do wszystkich graczy informacje, ze gra rozpoczeta\n";
	nullPacket();
	O = "STR";
	for (int i = 0; i < players.size(); i++) 
	{
		I = players[i].id;
		packMessage();
		sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
	}

	//Recieving all ack from players
	for (int i = 0; i < players.size(); i++)
	{
		recvfrom(ListeningSocket, packet, packet_size, 0, &tmp.clAddr, &tmp.clientLength);
	}

	//Starting the game!!!
	guessGame();

	//Wait for clients do end game
	std::cout << "\nCzekam na graczy, az zakoncza rozgrywke\n";

	while (play > 0) 
	{
		tmp.clientLength = sizeof(tmp.clAddr);
		ZeroMemory(&tmp.clAddr, tmp.clientLength);
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, &tmp.clAddr, &tmp.clientLength);
		unboxPacket();
		
		int ajdentyfikejszyn = I;
		if (O == "FIN")
		{
			if (A != "ACK") 
			{
				sendACK(tmp);
				for (int i = 0; i < players.size(); i++)
				{
					if (ajdentyfikejszyn == players[i].id)
					{
						nullPacket();
						O = "FIN";
						A = "END";
						I = players[i].id;
						packMessage();
						sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
						play--;
						std::cout << "Graczy zostalo: " << play << "\n";
					}
				}
			}
		}
		else 
		{
			//do nothing probably trash ack
		}
	}
	std::cout << "\nKoniec! Sprzatam po sobie...\n";
	//Close the socket
	closesocket(ListeningSocket);
	
	//Cleanup winsock
	WSACleanup();
}
void Server::TimeCheck() {
	using namespace std::literals::chrono_literals;
	//std::cout << "Wywolano watek czasowy... \n";
	//Loop that works in the other thread until time's over
	while (remainTime>0) {
		if (beginTime % 10 == currTime() % 10)
		{
			std::cout << "Remaining time: " << remainTime << "s\n";
			for (int i = 0; i < players.size(); i++)
			{
				nullPacket();
				O = "TLF";
				I = players[i].id;
				L = remainTime;
				packMessage();
				sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
			}
		}
		if (wygranko) break;
		std::this_thread::sleep_for(1s);
		remainTime -= 1;
	}
	Time = false;
}

void Server::guessGame() {
	std::cout << "\nGame started\n";
	std::thread tim = std::thread([this] {this->TimeCheck(); });
	client tmp;
	tmp.clientLength = sizeof(tmp.clAddr);
	ZeroMemory(&tmp.clAddr, tmp.clientLength);
	while (Time)
	{
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, &tmp.clAddr, &tmp.clientLength);
		unboxPacket();

		if (O == "NUM")
		{
			if (A == "ACK") continue;
			
			//std::cout << "Tak dostalem liczbe\n";
			//When numb is correct
			sendACK(tmp);
			if (L == randNumb) {
				int identyfikejtor = I;
				for (int i = 0; i < players.size(); i++)
				{
					if (identyfikejtor == players[i].id)
					{
						nullPacket();
						O = "HIT";
						I = players[i].id;
						L = randNumb;
						packMessage();
						sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
					}
					else {
						nullPacket();
						O = "LOS";
						I = players[i].id;
						L = randNumb;
						packMessage();
						sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
					}
				}
				wygranko = true;
				break;
			}
			//When number is not correct
			else {
				for (int i = 0; i < players.size(); i++)
				{
					if (I == players[i].id) {
						//std::cout << "no nie, zla liczba\n";
						nullPacket();
						O = "NUM";
						A = "MIS";
						I = players[i].id;
						packMessage();
						sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
					}
				}
			}
		}
		else {
			//Do nothing probably ack
		}
	}
	tim.join();
	//When time's up send to all players
	if (!wygranko) {
		for (int i = 0; i < players.size(); i++)
		{
			nullPacket();
			O = "TUP";
			I = players[i].id;
			packMessage();
			sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
		}
		//Info that all lost
		for (int i = 0; i < players.size(); i++)
		{
			nullPacket();
			O = "ALS";
			I = players[i].id;
			packMessage();
			sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[i].clAddr, players[i].clientLength);
		}
	}
}
long long Server::currTime() {
	auto time = std::chrono::duration_cast< std::chrono::seconds >(std::chrono::system_clock::now().time_since_epoch()).count();
	return time;
}
void Server::doCopy() {
	OC = O;
	AC = A;
	IC = I;
	LC = L;
}
void Server::Backup() {
	O = OC;
	A = AC;
	I = IC;
	L = LC;
}
void Server::sendACK(client c) {
	A = "ACK";
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &c.clAddr, c.clientLength);
}
void Server::sendErr(client c) {
	nullPacket();
	O = "CON";
	A = "ERR";
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&c, sizeof(c));
}
void Server::addPlayers(client c) {
	//Add the new connection do the list of connected client
	nullPacket();
	O = "RID";
	A = "SID";
	std::cout << "Losuje id\n";
	c.id = rand() % 255 + 1;
	//Check if id is not in use
	if (play > 0) {
		std::cout << "Wchodze w losowanie id ze sprawdzeniem\n";
		for (int i = 0; i < players.size()-1; i++) {
			if (c.id == players[i].id) {
				i = 0;
				c.id = rand() % 255 + 1;
			}
		}
	}
	I = c.id;
	players.push_back(c);
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, &players[play].clAddr, players[play].clientLength);
	std::cout << "\nSent to client " << play << " id " << players[play].id << "\n";
	play++;
	}
void Server::unboxPacket() {
	//std::cout << "Unboxing packet\n";
	std::string tmp = packet;
	//std::cout << "What i've got: " << tmp << "\n";
	O = tmp.substr(tmp.find("OP?") + 3, 3);
	//std::cout << "\nPo o: " << O;
	A = tmp.substr(tmp.find("OD?") + 3, 3);
	//std::cout << "\nPo a: " << A;
	ii = tmp.substr(tmp.find("ID?") + 3, tmp.find("ID?") + 3 - tmp.find("NR?"));
	std::stringstream hi(ii);
	hi >> I;
	//std::cout << "\nPo i: " << I;
	ll = tmp.substr(tmp.find("NR?") + 3, tmp.find("NR?") + 3 - tmp.find("TM?"));
	std::stringstream hl(ll);
	hl >> L;
	//std::cout << "\nPo l: " << L;
	//6std::cout << "\n Packet unboxed: " << O << " " << A << " " << I << " " << L << "\n";
}
void Server::packMessage()
{
	//Operation
	Packet = "OP?" + O + "<<";
	//Answer
	Packet += "OD?" + A + "<<";
	//ID number
		ii = std::to_string(I);
		Packet += "ID?" + ii + "<<";
	//Number
	if (L != 0) {
		ll = std::to_string(L);
		Packet += "NR?" + ll + "<<";
	}
	//std::cout << "\n" << currTime() << "\n";
	//Time
	Packet += "TM?" + std::to_string(currTime());
	Packet += "<<";

	//std::cout << "Packed message: " << Packet << "\n";
}
void Server::nullPacket()
{
	memset(packet, 0, sizeof(packet));
	O = "";
	A = "";
	L = 0;
	I = 0;
}
void Server::PrintData()
{
	std::cout << O << " " << A << " " << I << " " << L << " " << "\n";
}
void Server::sendToWait() {
	O = "WAT";
	I = players[play].id;
}