#pragma once
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <bitset>
#include <string>
#include <ctime>
#include <array>
#include <vector>
#include <chrono>
#include <sstream> 
#pragma comment(lib, "Ws2_32.lib")
#include <random>

class Server {
public:
	//Things connected with sockets
	WSADATA wsData;
	sockaddr_in serverHint, cli;
	WORD ver;
	int wsOk;
	SOCKET ListeningSocket, AcceptSocket;
	int sin_size = sizeof(sockaddr_in);
	int bytesRecieved;
	int cliLength = sizeof(cli);
	struct client {
		int id;
		SOCKADDR clAddr;
		int clientLength;
	};
	int play = 0;
	int ready = 0;
	client tmp;
	//Program
	int randNumb;
	std::vector<client> players;
	bool wygranko = false;
	bool READY = false;
	// Time stuff
	long long beginTime, durTime, remainTime;
	long long currTime();
	bool Time = true;

	//Packet things
	const int packet_size = 64;
	char packet[64];
	std::string Packet;

	//Option answer, id, nimber
	std::string O, A, ii="0", ll;
	int I = 0, L = 0;
	
	//Copy info
	std::string OC;
	std::string AC;
	int IC;
	int LC;

	//Functions
	void sendToWait();
	void TimeCheck();
	void guessGame();
	void doCopy();
	void Backup();
	void addPlayers(client c);
	void sendACK(client c);
	void sendErr(client c);
	void nullPacket();
	void unboxPacket();
	void packMessage();
	void PrintData();
	int bitToInteger(const std::string&str);
	void deletePlayer(int n);
	void main();
};
