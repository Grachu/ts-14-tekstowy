#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "server.h"
#include "client.h"
#pragma comment(lib, "Ws2_32.lib")

int main() {
	std::cout << "Wybierz:\ns - server,\nc - client\n";
	char a = 'b';
	while (a != 'c' || a != 'C' || a != 's' || a != 'S') {
		std::cin >> a;
		if (a == 'c' || a == 'C') {
			Client c;
			c.main();
		}
		if (a == 's' || a == 'S') {
			Server ser;
			ser.main();
		}
	}
	getchar();
}