#pragma once
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <bitset>
#include <string>
#include <chrono>
#include <ctime>
#pragma comment(lib, "Ws2_32.lib")
#include <random>
#include <thread>

class Client {
public:
	//Things connected with socket
	WSADATA wsData;
	sockaddr_in serverHint;
	WORD ver;
	int wsOk;
	SOCKET ListeningSocket;
	int sin_size = sizeof(sockaddr_in);
	int bytesRecieved;
	int id = 0;

	//Packet tings
	const int packet_size = 64;
	char packet[64];
	std::string Packet;

	//Option answer, id, number
	std::string O, A, ii="0", ll;
	int I = 0, L = 0;

	//Copy info
	std::string OC;
	std::string AC;
	int IC;
	int LC;


	//Thread tings
	bool is_finished = false;

	void doSending();
	long long currTime();
	void nullPacket();
	void unboxPacket();
	void packMessage();
	void PrintData();
	
	void doCopy();
	void Backup();

	void sendACK();
	void main();
};