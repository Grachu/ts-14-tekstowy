#include "server.h"
#include "client.h"

void Client::main() 
{
	std::cout << "Client \n";

	//Initialize winsock
	wsData;
	ver = MAKEWORD(2, 2);
	wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0) {
		std::cerr << "Cant start winsock! \n";
		return;
	}

	//Create a listening socket
	ListeningSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (ListeningSocket == INVALID_SOCKET) {
		std::cerr << "Cant create a socket! Quitting... \n";
		return;
	}

	//Create hint structure for the server
	serverHint.sin_addr.S_un.S_addr = ADDR_ANY;
	serverHint.sin_family = AF_INET;
	serverHint.sin_port = htons(53333);
	inet_pton(AF_INET, "192.168.43.223", &serverHint.sin_addr);

	std::cout << "Klient ruszyl. \n";
	std::cout << "Proba dolaczenia do gry\n";
	
	//Sending join request
	nullPacket();
	O = "CON", A = "";
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&serverHint, sin_size);
	PrintData();
	//Receiving ACK
	nullPacket();
	recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
	
	//Sending ID request
	nullPacket();
	O = "RID", A = "";
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(),  0, (sockaddr*)&serverHint, sin_size);
	PrintData();
		//Receiving ACK
	recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);

		//Receiving ID
	while (id == 0) {
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		id = I;
		nullPacket();
	}
	std::cout << "Otrzymano id!: " << id << std::endl;
	//Client's last ID response
	O = "RID";
	sendACK();
	nullPacket();

	//Sending RDY to server
	std::cout << "Wcisnij enter, aby potwierdzic gotowosc. \n";
	std::cin.get();
	std::cin.get();

	while (A != "ACK") {
		O = "RDY", A = "";
		packMessage();
		sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&serverHint, sin_size);
		//Receiving "RDY" ACK
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
	}
	std::cout << "Wyslano gotowosc! (RDY) \n";
	//Receiving "STR"
	while (O != "STR") {
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		if (O == "STR" && I == id) {
			sendACK();
			break;
		}
	}
	std::cout << "Otrzymano (STR) Gra powinna wystartowac \n";
	// Starting Sending Thread
	std::thread worker = std::thread([this] {this->doSending(); });
	
	std::cout << "Watek wysylajacy wystartowal! \n";
	//*Server starts game* 
	std::cout << "Rozpoczeto gre! \n\n";

	while (true) 
	{	
		// Server's response listening
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();

			//If Time info
		if (O == "TLF" && I == id)
		{
			std::cout << "Pozostaly czas: " << L << " sekund." << std::endl;
			sendACK();
		}
			//If missed number
		else if (O == "NUM" && A == "MIS" && I == id) 
		{
			std::cout << "Odpowiedz bledna. \n";
			O = "MIS";
			sendACK();
		}
			//If Won/Lost
				//If Won
			else if (O == "HIT" && I == id)
		{
				std::cout << "Udalo sie wygrac! \n";
				sendACK();
				break;
		}
				//If lost
			else if (O == "LOS" && I == id)
			{
				std::cout << "Niestety przegranko.. Przeciwnik wygral. \n";
				sendACK();
				break;
			}
			
			//If time's UP -> ALL lose
		else if ((O == "TUP" && I == id) || (O == "ALS" && I == id))
			{
				std::cout << "Koniec czasu! Nikt nie wygral \n";
				sendACK();
				break;
			}
		else 
		{ 
			// do nothing prob. ACK 
		}
	}

	// Pzerwanie w�tku
	is_finished = true;

	std::cout << "Wprowadz dowolna liczbe aby przejsc dalej...\n";
	worker.join();
	std::cout << "Koniec watku wysylajacego \n";
	// End of the game, disconnecting
	nullPacket();
	O = "FIN";
	packMessage();
	std::cout << "Wcisnij enter, aby zakonczyc polaczenie z serwerem. \n";
	std::cin.get();
	std::cin.get();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&serverHint, sin_size);
	std::cout << "Wyslano info o rozlaczeniu. \n";
	
	// Receiving "ACK"
	recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
	// Receiving "END"
	while (A != "END") 
	{
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
	}
	std::cout << "Server odebral info o rozlaczeniu. \n";
	sendACK();
	
	// Finally
	std::cout << "Aby zakonczyc wcisnij klawisz enter.\n" << std::endl;
	std::cin.get();

	// Close the socket
	closesocket(ListeningSocket);
	// Cleanup Winsock
	WSACleanup();
}

void Client::doSending()
{
	using namespace std::literals::chrono_literals;
	std::cout << "Rozpoczeto watek wysylajacy!" << std::endl;
	std::string answer;
	while (!is_finished) {
		std::cin >> answer;
		while (answer.size() > 9 || atoi(answer.c_str()) <= 0 || atoi(answer.c_str()) > 255)
		{
			std::cout << "Blad! Podaj liczbe z zakresu 0-255\n";
			std::cin >> answer;
		}
		// Sending answer
		nullPacket();
		O = "NUM";
		A = "";
		I = id;
		L = atoi(answer.c_str());
		packMessage();
		sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&serverHint, sin_size);
		//std::cout << "\nsent "<< answer <<"\n";
		nullPacket();
		std::this_thread::sleep_for(0.5s);
	}
	if (is_finished) {
		//std::cout << "\nXd\n";
		return;
	}
}

void Client::doCopy() {
	OC = O;
	AC = A;
	IC = I;
	LC = L;
}
void Client::Backup() {
	O = OC;
	A = AC;
	I = IC;
	L = LC;
}
void Client::sendACK() {
	A = "ACK";
	packMessage();
	sendto(ListeningSocket, Packet.c_str(), Packet.size(), 0, (sockaddr*)&serverHint, sin_size);
}

void Client::unboxPacket() {
	std::string tmp = packet;
	//std::cout << "What i've got: " << tmp << "\n";
	O = tmp.substr(tmp.find("OP?") + 3, 3);
	//std::cout << "\nPo o: " << O;
	A = tmp.substr(tmp.find("OD?") + 3, 3);
	//std::cout << "\nPo a: " << A;
	ii = tmp.substr(tmp.find("ID?") + 3, tmp.find("ID?") + 3 - tmp.find("NR?"));
	std::stringstream hi(ii);
	hi >> I;
	//std::cout << "\nPo i: " << I;
	ll = tmp.substr(tmp.find("NR?") + 3, tmp.find("NR?") + 3 - tmp.find("TM?"));
	std::stringstream hl(ll);
	hl >> L;
	//std::cout << "\nPo l: " << L;
	//6std::cout << "\n Packet unboxed: " << O << " " << A << " " << I << " " << L << "\n";
}
void Client::packMessage()
{
	//Operation
	Packet = "OP?" + O + "<<";
	//Answer
	Packet += "OD?" + A + "<<";
	//ID number
	ii = std::to_string(I);
	Packet += "ID?" + ii + "<<";
	//Number
	if (L != 0) {
		ll = std::to_string(L);
		Packet += "NR?" + ll + "<<";
	}
	Packet += "TM?" + std::to_string(currTime());
	Packet += "<<";
	//std::cout << "\n" << currTime() << "\n";
	//Time
	//	
	//
	//std::cout << "Packed message: " << Packet << "\n";
}
void Client::nullPacket()
{
	memset(packet, 0, sizeof(packet));
	O = "";
	A = "";
	L = 0;
}

void Client::PrintData()
{
	std::cout << O << " " << A << " " << I << " " << L << " " << "\n";
}
long long Client::currTime() {
	auto time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	return time;
}